class QController < R00lz::Controller
  def a_quote
    "Hey there! I'm a quote!"
  end

  def shakes
    @noun = :winking # NOTE: gets implicitely passed through into the render method
    render(:shakes)
  end

  def card_trick
    n = params["card"] || "Queen" # NOTE: you can pass a param to the URL: http://localhost:3000/q/card_trick?card=3
    "Your card: the #{n} of spades!"
  end

  def fq # file quote
    @q = FileModel.find(params["q"] || 1) # NOTE: @q is passed through in the binding
    STDERR.puts("MISSY LOOK params = #{params}")
    render :quote # NOTE: refers to erb file quote.html.erb
  end
end